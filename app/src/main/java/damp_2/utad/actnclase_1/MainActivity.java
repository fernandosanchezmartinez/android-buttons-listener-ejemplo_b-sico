package damp_2.utad.actnclase_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    public EditText edtxtNombre = null;
    public EditText edtxtEmail = null;
    public EditText edtxtTelefono = null;
    public EditText edtxtDireccion = null;

    public LinearLayout ventana1=null;
    public LinearLayout ventana2=null;

    public Button btnVolver=null;
    public Button btnEditar=null;
    public Button btnperfil=null;
    public Button btnAnterior=null;
    public Button btnSiguiente=null;



    ButtonListeners btnlis1;


    public void init_buttons() {
        btnlis1=new ButtonListeners(this);

        btnVolver = (Button) findViewById(R.id.btnVolver);
        btnEditar = (Button) findViewById(R.id.btnEditar);

        btnperfil = (Button) findViewById(R.id.btnperfil);
        btnAnterior = (Button) findViewById(R.id.btnAnterior);
        btnSiguiente = (Button) findViewById(R.id.btnSiguiente);

        btnVolver.setOnClickListener(btnlis1);
        btnEditar.setOnClickListener(btnlis1);

        btnperfil.setOnClickListener(btnlis1);
        btnAnterior.setOnClickListener(btnlis1);
        btnSiguiente.setOnClickListener(btnlis1);
    }

    public void init_text(){
        edtxtNombre=(EditText) findViewById(R.id.edtxtNombre);
        edtxtEmail=(EditText) findViewById(R.id.edtxtEmail);
        edtxtTelefono=(EditText) findViewById(R.id.edtxtTelefono);
        edtxtDireccion=(EditText) findViewById(R.id.edtxtDireccion);

    }

    public void initLayouts(){
        ventana1=(LinearLayout)findViewById(R.id.ventana1);
        ventana2=(LinearLayout)findViewById(R.id.ventana2);

    }

    public void cambiarPag() {
        ventana1.setVisibility(View.GONE);
        ventana2.setVisibility(View.VISIBLE);
    }

    public void hacerEditable() {

        btnEditar.setText("GUARDAR");
        btnVolver.setText("CANCELAR");

        edtxtNombre.setEnabled(true);
        edtxtEmail.setEnabled(true);
        edtxtTelefono.setEnabled(true);
        edtxtDireccion.setEnabled(true);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initLayouts();
        this.init_buttons();
        this.init_text();
    }


}
