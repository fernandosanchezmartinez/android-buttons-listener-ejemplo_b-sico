package damp_2.utad.actnclase_1;


import android.view.View;
import android.widget.Button;
import java.lang.Override;

public class ButtonListeners implements View.OnClickListener{

    MainActivity ma;

    public ButtonListeners(MainActivity ma1){
        ma=ma1;

    }

    @Override
    public void onClick(View v){

        Button btnTarget = (Button)v;
            if (btnTarget.getId() == (R.id.btnVolver)){

            ma.cambiarPag();
             }

        if (btnTarget.getId() == (R.id.btnEditar)){

            ma.hacerEditable();
        }



    }
}
